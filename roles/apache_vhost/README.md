apache_vhost
=========

This is simple role to configure `apache` virtual host. It supports only subdomains of host domain, so that we can use `ansible_facts['domain']` in templates as much as possible :)

Role Variables
--------------

`vhost_name: "www"` - virtial host name

`server_admin: admin@{{ vhost_name }}.{{ ansible_domain }}` - virtual host admin e-mail

Dependencies
------------

Role does not have any dependencies.

Example Playbook
----------------

To get virtual host `test.example.com` on your node `webserver.example.com` in group `lamp`:

    - hosts: lamp
      roles:
         - { role: apache_vhost, vhost_name: "test", server_admin: "tester@example.com" }

License
-------

BSD

Author Information
------------------

Author: Edgar Kalaev.

For EPAM DevOps Spring.
